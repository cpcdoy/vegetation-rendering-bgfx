$input v_pos, v_view, v_normal, v_texcoord0, v_shadowcoord

#include "../common/common.sh"

#define Sampler sampler2DShadow
SAMPLER2D(s_tex,  0);
SAMPLER2DSHADOW(u_shadowMap, 1);

uniform vec4 u_lightPos;


uniform vec4 u_time;

vec2 lit(vec3 _ld, vec3 _n, vec3 _vd, float _exp)
{
	//diff
	float ndotl = dot(_n, _ld);

	//spec
	vec3 r = 2.0*ndotl*_n - _ld; //reflect(_ld, _n);
	float rdotv = dot(r, _vd);
	float spec = step(0.0, ndotl) * pow(max(0.0, rdotv), _exp) * (2.0 + _exp)/8.0;

	return max(vec2(ndotl, spec), 0.0);
}

float hardShadow(Sampler _sampler, vec4 _shadowCoord, float _bias)
{
	vec3 texCoord = _shadowCoord.xyz/_shadowCoord.w;
	return shadow2D(_sampler, vec3(texCoord.xy, texCoord.z-_bias) );
}

float PCF(Sampler _sampler, vec4 _shadowCoord, float _bias, vec2 _texelSize)
{
	vec2 texCoord = _shadowCoord.xy/_shadowCoord.w;

	bool outside = any(greaterThan(texCoord, vec2_splat(1.0)))
				|| any(lessThan   (texCoord, vec2_splat(0.0)))
				 ;

	if (outside)
	{
		return 1.0;
	}

	float result = 0.0;
	vec2 offset = _texelSize * _shadowCoord.w;

	result += hardShadow(_sampler, _shadowCoord + vec4(vec2(-1.5, -1.5) * offset, 0.0, 0.0), _bias);
	result += hardShadow(_sampler, _shadowCoord + vec4(vec2(-1.5, -0.5) * offset, 0.0, 0.0), _bias);
	result += hardShadow(_sampler, _shadowCoord + vec4(vec2(-1.5,  0.5) * offset, 0.0, 0.0), _bias);
	result += hardShadow(_sampler, _shadowCoord + vec4(vec2(-1.5,  1.5) * offset, 0.0, 0.0), _bias);

	result += hardShadow(_sampler, _shadowCoord + vec4(vec2(-0.5, -1.5) * offset, 0.0, 0.0), _bias);
	result += hardShadow(_sampler, _shadowCoord + vec4(vec2(-0.5, -0.5) * offset, 0.0, 0.0), _bias);
	result += hardShadow(_sampler, _shadowCoord + vec4(vec2(-0.5,  0.5) * offset, 0.0, 0.0), _bias);
	result += hardShadow(_sampler, _shadowCoord + vec4(vec2(-0.5,  1.5) * offset, 0.0, 0.0), _bias);

	result += hardShadow(_sampler, _shadowCoord + vec4(vec2(0.5, -1.5) * offset, 0.0, 0.0), _bias);
	result += hardShadow(_sampler, _shadowCoord + vec4(vec2(0.5, -0.5) * offset, 0.0, 0.0), _bias);
	result += hardShadow(_sampler, _shadowCoord + vec4(vec2(0.5,  0.5) * offset, 0.0, 0.0), _bias);
	result += hardShadow(_sampler, _shadowCoord + vec4(vec2(0.5,  1.5) * offset, 0.0, 0.0), _bias);

	result += hardShadow(_sampler, _shadowCoord + vec4(vec2(1.5, -1.5) * offset, 0.0, 0.0), _bias);
	result += hardShadow(_sampler, _shadowCoord + vec4(vec2(1.5, -0.5) * offset, 0.0, 0.0), _bias);
	result += hardShadow(_sampler, _shadowCoord + vec4(vec2(1.5,  0.5) * offset, 0.0, 0.0), _bias);
	result += hardShadow(_sampler, _shadowCoord + vec4(vec2(1.5,  1.5) * offset, 0.0, 0.0), _bias);

	return result / 16.0;
}

vec2 blinn(vec3 _lightDir, vec3 _normal, vec3 _viewDir)
{
	float ndotl = dot(_normal, _lightDir);
	vec3 reflected = _lightDir - 2.0*ndotl*_normal; // reflect(_lightDir, _normal);
	float rdotv = dot(reflected, _viewDir);
	return vec2(ndotl, rdotv);
}

float fresnel(float _ndotl, float _bias, float _pow)
{
	float facing = (1.0 - _ndotl);
	return max(_bias + (1.0 - _bias) * pow(facing, _pow), 0.0);
}

vec4 lit(float _ndotl, float _rdotv, float _m)
{
	float diff = max(0.0, _ndotl);
	float spec = step(0.0, _ndotl) * max(0.0, _rdotv * _m);
	return vec4(1.0, diff, spec, 1.0);
}

void main()
{
	/*vec3 lightDir = vec3(0.0, 0.0, -1.0);
	vec3 normal = normalize(v_normal);
	vec3 view = normalize(v_view);
	vec2 bln = blinn(lightDir, normal, view);
	vec4 lc = lit(bln.x, bln.y, 1.0);
	float fres = fresnel(bln.x, 0.2, 5.0);

	float index = ( (sin(v_pos.x*3.0+u_time.x)*0.3+0.7)
				+ (  cos(v_pos.y*3.0+u_time.x)*0.4+0.6)
				+ (  cos(v_pos.z*3.0+u_time.x)*0.2+0.8)
				)*M_PI;

	vec3 color = vec3(sin(index*8.0)*0.4 + 0.6
					, sin(index*4.0)*0.4 + 0.6
					, sin(index*2.0)*0.4 + 0.6
					) * v_color0.xyz;

	gl_FragColor.xyz = pow(vec3(0.07, 0.06, 0.08) + color*lc.y + fres*pow(lc.z, 128.0), vec3_splat(1.0/2.2) );
	gl_FragColor.w = 1.0;*/



	vec4 color = toLinear(texture2D(s_tex, v_texcoord0));

	//float depth = v_position.z/v_position.w * 0.5 + 0.5;

	if (color.a > 0.5)
	{
		vec3 normal = normalize(cross(dFdx(v_view), dFdy(v_view) ) );
		vec3 lightDir = vec3(0.0, 0.0, 0.0);
		float ndotl = max(dot(normal, lightDir), 0.7);
		float spec = pow(ndotl, 10000.0);

		vec3 v  = v_view;
		vec3 vd = -normalize(v);
		vec3 n  = v_normal;
		vec3 l  = u_lightPos.xyz;
		float cosTheta = dot(n, l);
	
		float shadowMapBias = 0.005*tan(acos(cosTheta));
		shadowMapBias = clamp(shadowMapBias, 0, 0.01);
		vec3 ld = -normalize(l);

		vec2 lc = lit(ld, n, vd, 1.0);

		vec2 texelSize = vec2_splat(1.0/512.0);
		float visibility = hardShadow(u_shadowMap, v_shadowcoord, shadowMapBias);
		//float visibility = PCF(u_shadowMap, v_shadowcoord, shadowMapBias, texelSize);

		vec3 ambient = 0.1 * color.rgb;
		vec3 brdf = (lc.x + lc.y) * color.rgb * visibility;

		vec3 final = toGamma(abs(ambient + brdf));
		
		//gl_FragColor = lit(lightDir.x, lightDir.y, 1.0) * pow(pow(color, vec4_splat(2.2) ) * ndotl + spec, vec4_splat(1.0/2.2) );
		gl_FragColor = vec4(final, 1.0);
	}
	else
		gl_FragColor = color;
}