#include "common.h"
#include "bgfx_utils.h"

#include <bgfx.h>
#include <bx/readerwriter.h>
#include "entry/entry.h"
#include "camera.h"

int _main_(int /*_argc*/, char** /*_argv*/)
{
	uint32_t width = 1280;
	uint32_t height = 720;
	uint32_t debug = BGFX_DEBUG_TEXT;
	uint32_t reset = BGFX_RESET_VSYNC;

	bgfx::init(bgfx::RendererType::Direct3D11);
	bgfx::reset(width, height, reset);

	bgfx::setDebug(debug);

	float at[3] = { 0.0f, 0.0f,  0.0f };
	float eye[3] = { 0.0f, 10.0f, -10.5f };
	float initialPos[3] = { 0.0f, 18.0f, -40.0f };
	float viewM[16];

	cameraCreate();
	cameraSetPosition(eye);
	cameraSetVerticalAngle(-0.35f);
	cameraGetViewMtx(viewM);

	bgfx::setViewClear(0
		, BGFX_CLEAR_COLOR | BGFX_CLEAR_DEPTH
		, 0xffffffff
		, 1.0f
		, 0
		);

	// Uniforms.
	bgfx::UniformHandle u_shadowMap = bgfx::createUniform("u_shadowMap", bgfx::UniformType::Int1);
	bgfx::UniformHandle u_lightPos = bgfx::createUniform("u_lightPos", bgfx::UniformType::Vec4);
	bgfx::UniformHandle u_lightMtx = bgfx::createUniform("u_lightMtx", bgfx::UniformType::Mat4);
	bgfx::UniformHandle u_time = bgfx::createUniform("u_time", bgfx::UniformType::Vec4);
	bgfx::UniformHandle s_tex = bgfx::createUniform("s_tex", bgfx::UniformType::Int1);

	bgfx::ProgramHandle program = loadProgram("vs_mesh", "fs_mesh");
	bgfx::ProgramHandle programFloor = loadProgram("vs_floor", "fs_floor");
	bgfx::ProgramHandle programShadow;
	bgfx::ProgramHandle programMesh;

	Mesh* mesh = meshLoad("meshes/grass_03.bin");
	Mesh* hollowcube = meshLoad("meshes/hollowcube.bin");

	bgfx::TextureHandle grassTex = loadTexture("leafs1.dds");
	bgfx::TextureHandle shadowMapTexture;

	uint16_t shadowMapSize = 512;

	bgfx::FrameBufferHandle shadowMapFB;

		programShadow = loadProgram("vs_sms_shadow", "fs_sms_shadow");
		programMesh = loadProgram("vs_sms_mesh", "fs_sms_mesh");

		shadowMapTexture = bgfx::createTexture2D(shadowMapSize, shadowMapSize, 1, bgfx::TextureFormat::D16, BGFX_TEXTURE_COMPARE_LEQUAL);
		bgfx::TextureHandle fbtextures[] = { shadowMapTexture };
		shadowMapFB = bgfx::createFrameBuffer(BX_COUNTOF(fbtextures), fbtextures, true);
	

	int64_t timeOffset = bx::getHPCounter();

	const uint64_t stateCommon = 0
		| BGFX_STATE_RGB_WRITE
		| BGFX_STATE_ALPHA_WRITE
		| BGFX_STATE_DEPTH_TEST_LESS
		| BGFX_STATE_MSAA
		;

	const uint64_t stateTransparent = stateCommon
		| BGFX_STATE_BLEND_ALPHA
		;


	#define RENDER_SHADOW_PASS_ID 0
	#define RENDER_SCENE_PASS_ID  1

	MeshState* states[3];
	//Grass w/ tex
	states[0] = meshStateCreate();
	states[0]->m_state = stateTransparent;
	states[0]->m_viewId = RENDER_SCENE_PASS_ID;
	states[0]->m_program = program;
	states[0]->m_numTextures = 2;
	states[0]->m_textures[0].m_flags = UINT32_MAX;
	states[0]->m_textures[0].m_stage = 1;
	states[0]->m_textures[0].m_sampler = u_shadowMap;
	states[0]->m_textures[0].m_texture = shadowMapTexture;
	states[0]->m_textures[1].m_flags = UINT32_MAX;
	states[0]->m_textures[1].m_stage = 0;
	states[0]->m_textures[1].m_sampler = s_tex;
	states[0]->m_textures[1].m_texture = grassTex;

	//Deth
	states[1] = meshStateCreate();
	states[1]->m_state = 0
		| BGFX_STATE_RGB_WRITE
		| BGFX_STATE_ALPHA_WRITE
		| BGFX_STATE_DEPTH_WRITE
		| BGFX_STATE_DEPTH_TEST_LESS
		| BGFX_STATE_CULL_CW
		| BGFX_STATE_MSAA
		;
	states[1]->m_program = programShadow;
	states[1]->m_viewId = RENDER_SHADOW_PASS_ID;
	states[1]->m_numTextures = 0;

	//Mesh w/o tex
	states[2] = meshStateCreate();
	states[2] = meshStateCreate();
	states[2]->m_state = 0
		| BGFX_STATE_RGB_WRITE
		| BGFX_STATE_ALPHA_WRITE
		| BGFX_STATE_DEPTH_WRITE
		| BGFX_STATE_DEPTH_TEST_LESS
		| BGFX_STATE_CULL_CCW
		| BGFX_STATE_MSAA
		;
	states[2]->m_program = programMesh;
	states[2]->m_viewId = RENDER_SCENE_PASS_ID;
	states[2]->m_numTextures = 1;
	states[2]->m_textures[0].m_flags = UINT32_MAX;
	states[2]->m_textures[0].m_stage = 0;
	states[2]->m_textures[0].m_sampler = u_shadowMap;
	states[2]->m_textures[0].m_texture = shadowMapTexture;

	// Time acumulators.
	float timeAccumulatorLight = 0.0f;
	float timeAccumulatorScene = 0.0f;

	entry::MouseState mouseState;
	while (!entry::processEvents(width, height, debug, reset, &mouseState))
	{
		bgfx::setViewRect(0, 0, 0, width, height);

		bgfx::touch(0);

		int64_t now = bx::getHPCounter();
		static int64_t last = now;
		const int64_t frameTime = now - last;
		last = now;
		const double freq = double(bx::getHPFrequency());
		const double toMs = 1000.0 / freq;
		float time = (float)((bx::getHPCounter() - timeOffset) / double(bx::getHPFrequency()));
		const float deltaTime = float(frameTime / freq);

		// Update time accumulators.
		timeAccumulatorLight += deltaTime;
		timeAccumulatorScene += deltaTime;

		bgfx::setUniform(u_time, &time);

		bgfx::dbgTextClear();
		bgfx::dbgTextPrintf(0, 3, 0x0f, "Frame: % 7.3f[ms]", double(frameTime)*toMs);

		cameraUpdate(deltaTime, mouseState);
		cameraGetViewMtx(viewM);

		//float view[16];
		//bx::mtxLookAt(viewM, eye, at);


		/*bgfx::setTexture(0, s_tex, grassTex);
		meshSubmit(mesh, 0, program, mtx, stateTransparent);*/

		//bx::mtxScale(mtx2, 20.0, 1.0, 20.0);

		//meshSubmit(floor, 0, programFloor, mtx2, BGFX_STATE_DEFAULT);


		// Setup lights.
		float lightPos[4];
		lightPos[0] = 10;
		lightPos[1] = 10;
		lightPos[2] = 10;
		lightPos[3] = 0.0f;

		bgfx::setUniform(u_lightPos, lightPos);

		// Define light matrices.
		float lightView[16];
		float lightProj[16];

		eye[0] = -lightPos[0];
		eye[1] = -lightPos[1];
		eye[2] = -lightPos[2];

		at[0] = 0.0f;
		at[1] = 0.0f;
		at[2] = 0.0f;

		bx::mtxLookAt(lightView, eye, at);

		const float area = 30.0f;
		bx::mtxOrtho(lightProj, -area, area, -area, area, -100.0f, 100.0f);

		bgfx::setViewRect(RENDER_SHADOW_PASS_ID, 0, 0, shadowMapSize, shadowMapSize);
		bgfx::setViewFrameBuffer(RENDER_SHADOW_PASS_ID, shadowMapFB);
		bgfx::setViewTransform(RENDER_SHADOW_PASS_ID, lightView, lightProj);

		float proj[16];
		bx::mtxProj(proj, 60.0f, float(width) / float(height), 0.1f, 1000.0f);
		bgfx::setViewTransform(RENDER_SCENE_PASS_ID, viewM, proj);

		bgfx::setViewRect(RENDER_SCENE_PASS_ID, 0, 0, width, height);

		// Clear backbuffer and shadowmap framebuffer at beginning.
		bgfx::setViewClear(RENDER_SHADOW_PASS_ID
			, BGFX_CLEAR_COLOR | BGFX_CLEAR_DEPTH
			, 0xffffffff, 1.0f, 0
			);

		bgfx::setViewClear(RENDER_SCENE_PASS_ID
			, BGFX_CLEAR_COLOR | BGFX_CLEAR_DEPTH
			, 0xffffffff, 1.0f, 0
			);

		// Render.
		float mtxShadow[16];
		float lightMtx[16];

		const float mtxCrop[16] =
		{
			0.5f, 0.0f, 0.0f, 0.0f,
			0.0f, 0.5f, 0.0f, 0.0f,
			0.0f, 0.0f, 0.5f, 0.0f,
			0.5f, 0.5f, 0.5f, 1.0f,
		};

		float mtxTmp[16];
		bx::mtxMul(mtxTmp, lightProj, mtxCrop);
		bx::mtxMul(mtxShadow, lightView, mtxTmp);

		float mtx[16];
		for (int i = -10; i < 20; i += 2)
		{
				bx::mtxTranslate(mtx, 0, 0, i);
				// Grass light matrix.
				bx::mtxMul(lightMtx, mtx, mtxShadow);
				time += 8.5f;
				float lastTime = time;
				bgfx::setUniform(u_lightMtx, lightMtx);
				bgfx::setUniform(u_time, &time);
				meshSubmit(mesh, &states[1], 1, mtx);
				bgfx::setUniform(u_lightMtx, lightMtx);
				bgfx::setUniform(u_time, &lastTime);
				meshSubmit(mesh, &states[0], 1, mtx);
		}


		float mtx2[16];
		bx::mtxSRT(mtx2
			, 2.5f, 2.5f, 2.5f
			, 0.0f, 1.56f - timeAccumulatorScene, 0.0f
			, 0.0f, 10.0f, 0.0f
			);

		bx::mtxMul(lightMtx, mtx2, mtxShadow);
		bgfx::setUniform(u_lightMtx, lightMtx);
		meshSubmit(hollowcube, &states[1], 1, mtx2);
		bgfx::setUniform(u_lightMtx, lightMtx);
		meshSubmit(hollowcube, &states[2], 1, mtx2);


		bgfx::frame();
	}

	meshUnload(mesh);
	meshUnload(hollowcube);
	cameraDestroy();

	meshStateDestroy(states[0]);
	meshStateDestroy(states[1]);
	meshStateDestroy(states[2]);

	bgfx::destroyProgram(program);
	bgfx::destroyProgram(programMesh);
	bgfx::destroyProgram(programShadow);

	bgfx::destroyUniform(s_tex);
	bgfx::destroyUniform(u_shadowMap);
	bgfx::destroyUniform(u_lightPos);
	bgfx::destroyUniform(u_lightMtx);
	bgfx::destroyUniform(u_time);

	bgfx::destroyFrameBuffer(shadowMapFB);

	bgfx::destroyTexture(grassTex);
	bgfx::destroyTexture(shadowMapTexture);

	bgfx::shutdown();

	return 0;
}
