$input a_position, a_normal, a_texcoord0
$output v_pos, v_view, v_normal, v_texcoord0, v_shadowcoord

#include "../common/common.sh"

uniform vec4 u_time;

uniform mat4 u_lightMtx;

void main()
{
	vec3 pos = a_position;
	float fact = 7/a_position.y;
	float sx = sin(pos.x*32.0+u_time.x*4.0)/fact*0.5+0.5;
	float cy = cos(pos.y*32.0+u_time.x*4.0)/fact*0.5+0.5;
	vec3 displacement = vec3(sx, cy, sx*cy);
	vec3 normal = a_normal.xyz*2.0 - 1.0;

	pos = pos + normal*displacement*vec3(0.06, 0.06, 0.06)*vec3(10.0, 3.0, 1.0);

	gl_Position = mul(u_modelViewProj, vec4(pos, 1.0) );
	v_pos = gl_Position.xyz;
	//v_view = mul(u_modelView, vec4(pos, 1.0) ).xyz;

	//v_normal = mul(u_modelView, vec4(normal, 0.0) ).xyz;

	float len = length(displacement)*0.4+0.6;
	//v_color0 = vec4(len, 1.0, 1.0, 1.0);

	v_texcoord0 = a_texcoord0;

	vec4 normal2 = vec4(a_normal, 1) * 2.0 - 1.0;
	v_normal = normalize(mul(u_modelView, vec4(normal2.xyz, 0.0) ).xyz);
	v_view = mul(u_modelView, vec4(a_position, 1.0)).xyz;

	const float shadowMapOffset = 0.001;
	vec3 posOffset = a_position + normal.xyz * shadowMapOffset;
	v_shadowcoord = mul(u_lightMtx, vec4(posOffset, 1.0) );
}
