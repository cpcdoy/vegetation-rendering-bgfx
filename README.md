# README #

### What is this repository for? ###

Vegetation rendering test using the BGFX library

### How do I get set up? ###

Compile BGFX.
Set up a VS project with the bgfx examples projects' VS settings.

Finally, to compile shaders, use GnuWin32's make for Windows:

"path to make.exe" TARGET=nb

Where nb is the shader backend

### How to run ? ###

Place your assets and source in the following way:

Folder tree:

-Vegetation rendering

-runtime

-->meshes

-->shaders

-->textures

After this, run the .exe in the runtime directory

### Who do I talk to? ###

cpcdoy (CpCd0y)